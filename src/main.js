import App from './App.svelte';
import axios from 'axios'
import { user } from './routes/_stores/auth'

// const apiUrl = 'https://v4.novalinkpix.com/';
const apiUrl = isProd ?  'https://v4.novalinkpix.com/' : 'http://nova-portal.test/';

axios.defaults.baseURL = apiUrl
// axios.defaults.withCredentials = true
axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'Accept': 'application/json'
}

axios.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, function (error) {
  console.log(error.response.status)
  if(error.response.status == 401){
    user.set(null)
  }
  return error;
});

const app = new App({
	target: document.body,
});

export default app;