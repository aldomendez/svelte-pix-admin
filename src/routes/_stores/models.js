import axios from 'axios'
import { addMessage } from './notifications'
import { id } from 'date-fns/locale'
import { get } from 'lodash'
import { writable } from 'svelte/store'

let definitions = {
    models: {
        company: {
            name: 'Company',
            table: 'companies',
            fields: [
                { field: 'id', label: 'Id', type: 'string', help: '', length: 191, size: 2 },
                { field: 'name', label: 'Name', type: 'string', help: '', length: 191, heading: true, size: 2 },
                { field: 'prefix', label: 'Prefix', type: 'string', help: '', length: 191, size: 2 },
                { field: 'parasql_id', label: 'ParaSQL Id', type: 'number', help: 'ID of this company in Parasql', size: 2 },
                { field: 'active', label: 'Active', type: 'checkbox', help: '', size: 2 },
            ],
            primary: 'id',
            visible: ['name', 'prefix', 'active', 'parasql_id'],
            editable: ['name', 'prefix', 'active', 'parasql_id'],
            formTabs: [{
                name: 'Generals',
                description: 'General company information',
                fields: ['name', 'description', 'active'],
            }],
            relations: {
                user: { by: 'id' }
            },
            controllers: {
                api: {
                    origin: 'api/company',
                    methods: ['index', 'store', 'update', 'show', 'destroy']
                }
            }
        },
        process: {
            table: 'processes',
            name: 'Process',
            fields: [
                { field: 'id', label: 'Id', type: 'string', help: '', length: 191, order: 1, size: 2 },
                { field: 'name', label: 'Name', type: 'string', help: '', length: 191, order: 2, required: true, heading: true, size: 2 },
                { field: 'description', label: 'Description', type: 'string', help: '', length: 191, order: 3, required: true, size: 2 },
                { field: 'active', label: 'Active', type: 'checkbox', help: '', order: 4, required: true, default: true, size: 2 },
                { field: 'configuration', label: 'Configuration', type: 'json', help: 'Contains configuration for shifts among others parameters for shift', length: 191, order: 5, size: 2 },
                { field: 'deleted_at', label: 'Deleted at', type: 'date', help: '', length: 191, order: 6, disabled: true, size: 2 },
                { field: 'created_at', label: 'Created at', type: 'date', help: '', length: 191, order: 6, disabled: true, size: 2 },
                { field: 'updated_at', label: 'Updated at', type: 'date', help: '', length: 191, order: 6, disabled: true, size: 2 },
            ],
            primary: 'id',
            visible: ['name', 'description', 'active'],
            editable: ['name', 'description', 'active'],
            defaults: {},
            formTabs: [{
                name: 'Generals',
                description: 'General company information',
                fields: ['name', 'description', 'active'],
            }],
            relations: {
                parts: {}
            },
            actions: {
                api: {
                    origin: 'api/process',
                    methods: ['index', 'store', 'update', 'show', 'destroy']
                }
            }
        },
        parts: {
            table: 'parts',
            name: 'Parts',
            fields: [
                { field: 'part', label: 'Part', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true, heading: true },
                { field: 'part_aka_customer', label: 'Customer Part', type: 'string', comment: 'varchar(191)', order: 1, size: 2, },
                { field: 'description', label: 'Description', type: 'string', comment: 'varchar(191)', order: 1, size: 4, required: true },
                { field: 'comments', label: 'Comments', type: 'string', comment: 'text', order: 1, size: 2, },
                { field: 'company_id', label: 'Company Id', type: 'string', comment: 'bigint(20)', order: 1, size: 2, required: true, },
                { field: 'type', label: 'Type', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true, },
                { field: 'category', label: 'Category', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true },
                { field: 'unit_of_measure_us', label: 'Unit of measure US', type: 'string', comment: 'varchar(191)', order: 1, size: 2, },
                { field: 'unit_of_measure_mx', label: 'Unit of measure MX', type: 'string', comment: 'varchar(191)', order: 1, size: 2, },
                { field: 'material_type', label: 'Material type', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true },
                { field: 'origin', label: 'Origin', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true, },
                { field: 'cost_us', label: 'Cost US', type: 'string', comment: 'varchar(191)', order: 1, size: 2, required: true, },
                { field: 'is_internal', label: 'Is for internal use?', type: 'checkbox', comment: 'tinyint(1)', order: 1, size: 2, },
                { field: 'created_by', label: 'Created_by', type: 'string', comment: 'int(10)', order: 1, size: 2, required: true, },
                { field: 'created_at', label: 'Created_at', type: 'string', comment: 'timestamp', order: 1, size: 2, },
                { field: 'updated_at', label: 'Updated_at', type: 'string', comment: 'timestamp', order: 1, size: 2, },
                { field: 'deleted_at', label: 'Deleted_at', type: 'string', comment: 'timestamp', order: 1, size: 2, },
            ],
            primary: 'part',
            visible: ['part', 'description', 'type', 'category',],
            editable: ['part', 'part_aka_customer', 'description', 'comments', 'company_id', 'type', 'category', 'unit_of_measure_us', 'unit_of_measure_mx', 'material_type', 'origin', 'cost_us', 'is_internal'],
            defaults: {},
            formTabs: [{
                name: 'Generals',
                description: 'General company information',
                fields: ['part', 'part_aka_customer', 'description', 'comments', 'company_id', 'type', 'category', 'unit_of_measure_us', 'unit_of_measure_mx', 'material_type', 'origin', 'cost_us', 'is_internal'],
            }],
            relations: {
                parts: {}
            },
            actions: {
                api: {
                    origin: 'api/process',
                    methods: ['index', 'store', 'update', 'show', 'destroy']
                }
            }
        }
    }
}

function catchWithMessage(err) {
    addMessage({
        type: "danger",
        title: "Something went wrong",
        message: err.response.data.message
    })
}

function prepareIds(ids) {
    return ids
}

const createApi = (model) => {
    return {
        index: (params) => axios.get(`api/resource/${model.table}`, { params }).catch(catchWithMessage),
        store: (values) => axios.post(`api/resource/${model.table}`, { values }).catch(catchWithMessage),
        show: (ids, params) => axios.get(`api/resource/${model.table}/${model.primary}/${prepareIds(ids)}`, { params }).catch(catchWithMessage),
        update: (ids, values) => axios.patch(`api/resource/${model.table}/${model.primary}/${prepareIds(ids)}`, { values }).catch(catchWithMessage),
        destroy: (ids) => axios.get(`api/resource/${model.table}/${model.primary}/${prepareIds(ids)}`).catch(catchWithMessage),
    }
}

const createParams = (model) => (p = { page: 1, limit: 10 }) => ({
    __include: [model.primary, ...model.visible].join(','),
    __limit: p.limit,
    __offset: p.page * p.limit - p.limit,
    __order: model.primary,
})

export {
    definitions,
    createApi,
    createParams,
}