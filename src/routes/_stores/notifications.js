import { writable } from 'svelte/store'

// API
let ex = {
    type: ['default', 'info', 'success', 'warning', 'danger'][2],
    title: '',
    message: '',
    duration: 5000,
}
let intervalRef;
const messages = writable([])

function addMessage(n) {
    messages.update(m => [...m, { ...ex, ...n, closeAt: window.performance.now() + (n.duration || ex.duration) }])
    
}
function deleteMessage(t) {
    messages.update(m => m.filter((_, i) => i !== t));
}
function clearAll() {
    messages.update(m => []);
}

(function deleteOldMessages() {
  intervalRef = setInterval(() => {
    messages.update(m => m.filter(m => m.closeAt > window.performance.now()));
  }, 1000);
})();


function stopChecking(){
    clearInterval(intervalRef);
    intervalRef = null
}



export {
    messages,
    addMessage,
    deleteMessage,
    clearAll
}