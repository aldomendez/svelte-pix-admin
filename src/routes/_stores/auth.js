import axios from 'axios'
import { get } from 'lodash'
import { writable } from 'svelte/store'

let fetchUserRequest

const user = writable(null)
const token = writable(get(localStorage, 'access_token', null))
const respError = writable(null)

token.subscribe(token => {
    localStorage.setItem('access_token', token)
    if (token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    } else {
        axios.defaults.headers.common['Authorization'] = null
    }
})

user.subscribe(user => { })

const log = () => { }

let fetchUser = function () {
    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.access_token}`
    fetchUserRequest =  axios.get('/api/user')
    
    fetchUserRequest.then(response => {
        user.set(response.data.user)
    }).catch(err => {
        console.log(err.response)
        if (err.response.status === 401) {
            respError.set(err.response.data)
        }
    }).finally(() => {

    })

    return fetchUserRequest
}

let login = function ({ email, password }) {
    respError.set(null)
    return axios.post('/api/login', { email, password })
        .then(res => {
            log(res)
            if (res.data.errors) {
                respError.set(res.data)
                log(res.data.errors)
            } else {
                let x = res.data.user
                user.set(x)
                token.set(x.api_token)
            }
        }).catch(err => {
            log(err)
        })
}

let logout = function () {
    return axios.post('api/logout').finally(() => {
        token.set(null)
        user.set(null)
    })
}

let recoverPassword = function ({ id }) {
    return axios.post('api/user/recoverid', { id })
        .then(res => {
            log(res)
        }).catch(err => {
            log(err)
        })
}

export {
    fetchUserRequest,
    fetchUser,
    login,
    logout,
    recoverPassword,
    user,
    token,
    respError,
}